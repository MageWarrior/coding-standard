# Introduction

MagePSR-2 is a Magento-friendly version of the [FIG PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md) coding standard intended for use with the [PHP_CodeSniffer (PHPCS)](https://github.com/squizlabs/PHP_CodeSniffer) tool by Squiz Labs. It incorporates the most of the built-in PSR-2 coding standard with the [Magento Coding Standard](https://github.com/magento-ecg/coding-standard) created by ECG and a sniff "borrowed" from the [WordPress Coding Standards](https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/) (the sniff ensures that the class name is in the correct format). Please note that some rules, such as the requirement for namespaces to be used, have been removed from the PSR-2 coding standard to make it compatible with Magento.

# Downloading

The MagePSR-2 Coding Standard can be found at the following address:
```
https://bitbucket.org/MageWarrior/coding-standard
```

# Prerequisites

1. PHP version 5.4 or greater
2. [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)
3. [ECG Magento Coding Standard](https://github.com/magento-ecg/coding-standard)

# Installation

Clone the repository to the path of your choice:
```bash
cd ~/coding-standards (or cd %USERPROFILE%\coding-standards on Windows)
git clone https://bitbucket.org/MageWarrior/coding-standard.git MageWarrior/
```

Alternatively, add a dependency to your project's `composer.json` file:
```json
{
    "require": {
        "magewarrior/coding-standard": "dev-master"
    }
}
```

# Usage

Run PHP_CodeSniffer:
```
phpcs --standard=/path/to/MagePSR-2 /path/to/code
```

# Contributing

Feel free to open a pull request to submit any changes that you would like to see or additions you would like to make and we'll consider merging them if we feel they are helpful.

# License

This coding standard is Public Domain...do whatever you want with it!

# Copyright

See above...no copyrights reserved! Enjoy!

# Author

[MageWarrior Development Team](http://magewarrior.us/)
